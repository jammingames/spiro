Shader "My/ColoredLine" {
	
	// A simple shader to draw lines 
	// (backface culling and lightning are turned off)
	
  //  Properties {
    	//_Color ("Main Color", Color) = (1,1,1,1)
    	//_BackColor ("Back Color", Color) = (1,1,1,1)
   // } 
    
    SubShader {
    	//Material {
    		//Diffuse [_Color]
    		//Ambient [_Color]
    	//}
        Pass {
        	ColorMaterial AmbientAndDiffuse
        	Blend SrcAlpha OneMinusSrcAlpha 
            ZWrite OFF
        	//Color [_Color]
            Lighting Off
            Cull Off
        }
        
      
    }
    
    //FallBack "VertexLit"
} 