﻿Shader "ColorPicker/ColorGradient" {
  Properties {
        _Color ("Color", Color) = (0,0,0)
        _endColor ("endColor", Color) = (1,1,1)
    }
	
	SubShader {
		Color [_Color]
		Color [_endColor]
	    Pass {	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"		
			
			// vertex input: position, UV
			struct appdata {
			    float4 vertex : POSITION;
			    float4 texcoord : TEXCOORD0;
			};
	
			struct pos_output {
			    float4 pos : SV_POSITION;
			    float4 uv : TEXCOORD0;
			};
			
			pos_output vert(appdata v) {
			    pos_output o;
			    o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			    o.uv = float4(v.texcoord.xy, 0, 0);
			    return o;
			}
			
			half4 frag(pos_output o) : COLOR {
				half p = floor(o.uv.x*1);
				half i = o.uv.x*1-p;
				half4 c = p == 0 ? half4(1, i, 0, 1) :
						           half4(1, 0, 0, 1);
			    return c;
			}
			ENDCG
	    }
	}
}

