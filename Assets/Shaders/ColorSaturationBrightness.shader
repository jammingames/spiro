Shader "ColorPicker/ColorSaturationBrightness" {
	Properties {
	    _Color ("Main Color", Color) = (1,1,1,1)
	     _MainTex ("First Texture", 2D) = "white" {}
	}
	SubShader {
	    Pass {	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			float4 _Color;
			sampler2D _MainTex;
			
			
			
			
			// vertex input: position, UV
			struct appdata {
			    float4 vertex : POSITION;
			    float4 texcoord : TEXCOORD0;
			};
	
			struct pos_output {
			    float4 pos : SV_POSITION;
			    float4 uv : TEXCOORD0;
			};
			
			pos_output vert(appdata v) {
			    pos_output o;
			    o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			    o.uv = float4(v.texcoord.xy, 0, 0);
			    return o;
			}
			
			half4 frag(pos_output o) : COLOR {
				half4 c = 1-o.uv.y + (_Color-1)*-o.uv.x;
			    return c;			
			}
			ENDCG
	    }
	}
}
