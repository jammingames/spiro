﻿using System.Collections;
using UnityEngine;

public static class AnimationExtensions
{
	public static bool IsPlaying (this Animator animator)
	{
		return animator.GetCurrentAnimatorStateInfo (0).length > animator.GetCurrentAnimatorStateInfo (0).normalizedTime;
	}
}
