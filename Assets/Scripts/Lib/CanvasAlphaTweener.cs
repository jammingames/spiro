﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasAlphaTweener : MonoBehaviour
{

	public float targetDuration = 1;
	public float currentAlpha = 1;
	public float targetAlpha = 1;
	public float currentLerpTime = 0;
	public EaseType easer;
	public float speed = 1;
	public float alpha = 1;
	float origAlpha;
	bool canMove = true;

	float range = 0;
	float start;
	CanvasGroup cGroup;

	void Awake ()
	{
		cGroup = GetComponent<CanvasGroup> ();
		origAlpha = cGroup.alpha;
		range = 0;
		start = range;

	}

	void OnEnable ()
	{

		currentAlpha = origAlpha;
		targetAlpha = currentAlpha;
		targetDuration = 1;

		start = currentAlpha;
		range = targetAlpha - start;

	}

	void Update ()
	{


		if (!canMove)
			return;
		currentAlpha = alpha;
		if (currentAlpha != targetAlpha) {
			currentLerpTime = Mathf.MoveTowards (currentLerpTime, targetDuration, Time.deltaTime);
			if (currentLerpTime > targetDuration) {
				currentLerpTime = targetDuration;
			}
			currentAlpha = Mathf.Lerp (start, targetAlpha, Ease.FromType (easer) (currentLerpTime / targetDuration));
			alpha = currentAlpha;
		}
		if (cGroup.alpha != alpha)
			cGroup.alpha = alpha;


	}

	public void ChangeAlpha (float target, float duration)
	{
		start = currentAlpha;

		targetAlpha = target;
		targetDuration = duration;
		range = targetAlpha - start;
		currentLerpTime = 0;
		easer = EaseType.Linear;
	}

	public void ChangeAlpha (float target, float duration, EaseType ease)
	{
		start = currentAlpha;
		targetAlpha = target;
		targetDuration = duration;

		range = targetAlpha - start;
		currentLerpTime = 0;
		easer = ease;
		////debug.log (targetDuration);
	}

	public void SetAlpha (float newAlpha, float newSpeed)
	{
		alpha = newAlpha;
		currentAlpha = newAlpha;
		origAlpha = newAlpha;
		speed = newSpeed;
	}

	public void Start ()
	{
		canMove = true;
	}

	public void Stop ()
	{
		canMove = false;
	}


}
