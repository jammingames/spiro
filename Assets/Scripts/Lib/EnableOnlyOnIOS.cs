﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnlyOnIOS : MonoBehaviour
{

	void Awake ()
	{
		#if UNITY_EDITOR || !UNITY_IPHONE || !UNITY_ANDROID
		gameObject.SetActive (false);
		#endif
	}

}
