﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour 
{
	private Text text;

	public string LocalString
	{
		get
		{
			if (text == null)
				text = GetComponent<Text>();
			return text.text;
		}
		set
		{
			if (text == null)
				text = GetComponent<Text>();

			text.text = value;
		}
	}
}
