﻿using UnityEngine;
using System.Collections;


public class SendEmail : MonoBehaviour
{

	public void SendEmailTo ()
	{
//		string email = "me@example.com";
		//email Id to send the mail to
		string email = "sales@invivo.com";
		//subject of the mail
		string subject = MyEscapeURL ("Google Cardboard Request");
		//body of the mail which consists of Device Model and its Operating System
		string body = MyEscapeURL ("Name: \nCompany: \nStreet: \nCity: \nState/Province: \nZip/Postal Code: \nNumber of kits requested: \n\n\n\n" +
						
		              "________" +
		              "\n\nPlease Do Not Modify This\n\n" +
		              "Model: " + SystemInfo.deviceModel + "\n\n" +
		              "OS: " + SystemInfo.operatingSystem + "\n\n" +
		              "________");
		//Open the Default Mail App
		Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL (url).Replace ("+", "%20");
	}

}