﻿using UnityEngine;
using System.Collections;

public class FadeOnStart : MonoBehaviour
{
	IEnumerator Start ()
	{
		yield return new WaitForSeconds (1.0f);
		Fader.instance.FadeAlphaOut (GetComponent<Renderer>().material, 2.0f, null);
	}
}
