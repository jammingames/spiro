﻿using UnityEngine;
using System.Collections;

public class ORef : MonoBehaviour
{
		static ORef _instance = null;

		public static ORef instance {
				get {
						if (!_instance) {
								// check if an FIX_ME is already available in the scene graph
								_instance = FindObjectOfType (typeof(ORef)) as ORef;

								// nope, create a new one
								if (!_instance) {
										var obj = new GameObject ("ORef");
										_instance = obj.AddComponent<ORef> ();
								}
						}

						return _instance;
				}
		}

		void OnApplicationQuit ()
		{
				// release reference on exit
				_instance = null;
		}

		public Camera mainCam, secondCam, guiCam;
		public Transform colorPickerPivot;
		public DrawLines dlines;
}
