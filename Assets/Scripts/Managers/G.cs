using System.Collections;
using UnityEngine;

public static class G
{
		public delegate void GameEvent ();

		public delegate void ColorChangeEvent (Color col,Color col2);

		public delegate void GamePause (bool isPaused);

		public delegate void RepositionEvent (Vector3 pos);

		public delegate void ResizeEvent (float pos);

		public static event RepositionEvent OnReposition;

		public static event ResizeEvent OnResize;

		public static event GameEvent GameStart, GameOver, OnCalibrate, OnReset, OnSave, OnClear;
		public static event ColorChangeEvent OnColorChange;
		public static event GamePause OnStartPause, OnGamePause, OnStartSwitchButton, OnSwitchButton, OnSwitchColor;

		public static void TriggerColorChange (Color col, Color col2)
		{
				if (OnColorChange != null)
						OnColorChange (col, col2);
			
		}

		public static void TriggerReposition (Vector3 pos)
		{
				if (OnReposition != null) {
						OnReposition (pos);
				}
		}



		public static void TriggerResize (float pos)
		{
				if (OnResize != null) {
						OnResize (pos);
				}
		}


		public static void TriggerGameStart ()
		{
				if (GameStart != null) {
						GameStart ();
				}
		}

		public static void TriggerStartPause (bool isPaused)
		{
				if (OnStartPause != null) {
						OnStartPause (isPaused);
				}
		}

		public static void TriggerStartSwitchButton (bool isOn)
		{
				if (OnStartSwitchButton != null) {
						OnStartSwitchButton (isOn);
				}
		}

		public static void TriggerGamePause (bool isPaused)
		{
				if (OnGamePause != null) {
						D.log ("pausing triggered");
						OnGamePause (isPaused);
				}
		}

		public static void TriggerSwitchButton (bool isPaused)
		{
				if (OnSwitchButton != null) {
						OnSwitchButton (isPaused);
				}
		}

		public static void TriggerSwitchColor (bool isPaused)
		{
				if (OnSwitchColor != null) {
						OnSwitchColor (isPaused);
				}
		}

		public static void TriggerGameOver ()
		{
				if (GameOver != null) {
						GameOver ();
				}
		}

		public static void TriggerCalibrate ()
		{
				if (OnCalibrate != null) {
						OnCalibrate ();
				}
		}

		public static void TriggerReset ()
		{
				if (OnReset != null) {
						OnReset ();
				}
		}

		public static void TriggerClear ()
		{
				if (OnClear != null) {
						OnClear ();
				}
		}

		public static void TriggerSave ()
		{
				if (OnSave != null) {
						OnSave ();
				}
		}
}
