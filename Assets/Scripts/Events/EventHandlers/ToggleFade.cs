﻿using UnityEngine;
using System.Collections;

public class ToggleFade : MonoBehaviour
{

		public float target;
		void Start ()
		{
				G.OnGamePause += OnPauseHandler;
		}


		void OnPauseHandler (bool isPaused)
		{
				if (isPaused) {
						Fader.instance.FadeAlphaIn (GetComponent<Renderer>().sharedMaterial, 1.0f, 0.4f, () => {
				
				
				
								D.log ("TriggerSwitchButton is " + isPaused);
						});
				} else {
			
						Fader.instance.FadeAlphaOut (GetComponent<Renderer>().sharedMaterial, target, 0.3f, () => {
						});
			
				}
		}
}
