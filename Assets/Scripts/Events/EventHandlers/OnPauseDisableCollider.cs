﻿using UnityEngine;
using System.Collections;

public class OnPauseDisableCollider : MonoBehaviour
{
	void Start ()
	{
		G.OnGamePause += DisableCollider;

	}

	void DisableCollider (bool isPaused)
	{
		GetComponent<Collider>().enabled = isPaused;
	}
}
