﻿using UnityEngine;
using System.Collections;

public class OnSwitchCameraColorChange : MonoBehaviour
{
		void Start ()
		{
				G.OnSwitchButton += OnSwitchButton;
		}

		void OnSwitchButton (bool isBackground)
		{
				if (isBackground) {
						D.log ("background color added ");
						G.OnColorChange += OnColorChangeEventHandler;
				} else {
						D.log ("background color removed ");
						G.OnColorChange -= OnColorChangeEventHandler;
				}
		}

		void OnColorChangeEventHandler (Color col, Color col2)
		{
				Camera.main.backgroundColor = col;

		}
}
