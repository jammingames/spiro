﻿using UnityEngine;
using System.Collections;

public class OnPauseFade : MonoBehaviour
{
		public Material mat;
		public Camera camCol;
		Color col;

		void Start ()
		{
				G.OnGamePause += OnPauseHandler;
				if (camCol) {
						col = camCol.backgroundColor;
						mat.color = col;
				}
				//Fader.instance.FadeAlphaOut (mat, 0.1f, null);
		}

		void OnPauseHandler (bool isPaused)
		{
				if (camCol) {
				
						col = camCol.backgroundColor;
						mat.color = col;
				
				}
				if (isPaused) {
						Fader.instance.FadeAlphaOut (mat, .8f, () => {
						
								
					
								D.log ("TriggerSwitchButton is " + isPaused);
						});
				} else {
				
						Fader.instance.FadeAlphaIn (mat, 0.7f, () => {
						});
						
				}
		}
}
