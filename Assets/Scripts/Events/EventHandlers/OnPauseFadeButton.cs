﻿using UnityEngine;
using System.Collections;

public class OnPauseFadeButton : MonoBehaviour
{
		public Material mat;
		public Camera camCol;
		Color col;

		void Start ()
		{
				G.OnStartSwitchButton += OnPauseHandler;
				if (camCol) {
						col = camCol.backgroundColor;
						mat.color = col;
				}
				//Fader.instance.FadeAlphaOut (mat, 0.1f, null);
		}

		void OnPauseHandler (bool isPaused)
		{
				
				Fader.instance.FadeAlphaOut (mat, .2f, () => {
						
						G.TriggerSwitchButton (isPaused);
						Fader.instance.FadeAlphaIn (mat, 0.3f, () => {
								D.log ("TriggerSwitchButton is " + isPaused);
						});
				});
		}
}
