using UnityEngine;

public class Draggable : MonoBehaviour
{
		public bool fixX;
		public bool fixY;
		Vector3 thumb;
		public Camera cam;
		bool dragging;

		void Update ()
		{
				if (Input.GetMouseButton (0)) {
					
						var ray = cam.ScreenPointToRay (Input.mousePosition);
						var point = cam.ScreenToWorldPoint (Input.mousePosition);
						RaycastHit hit;
						if (GetComponent<Collider>().Raycast (ray, out hit, 1000)) {
								
								
								dragging = true;

				
								point = GetComponent<Collider>().ClosestPointOnBounds (point);
								SetThumbPosition (point);
								if (GetComponent<Renderer> ())
										SendMessage ("OnDrag", Vector3.one - (thumb - GetComponent<Collider>().bounds.min) / GetComponent<Collider>().bounds.size.x);
						}
					
				}
				if (Input.GetMouseButtonUp (0)) {
						if (dragging)
								transform.parent.BroadcastMessage ("SendColor", SendMessageOptions.DontRequireReceiver);
						dragging = false;
						//print ("in get mouse up digabble broadcasts on color change. I believe i misused this");
				}
						
		}

		void SetDragPoint (Vector3 point)
		{
				if (GetComponent<Renderer> ())
						point = (Vector3.one - point) * GetComponent<Collider>().bounds.size.x + GetComponent<Collider>().bounds.min;
				SetThumbPosition (point);

		}

		void SetThumbPosition (Vector3 point)
		{		
				thumb = new Vector3 (fixX ? thumb.x : point.x, fixY ? thumb.y : point.y, thumb.z);
		}
}
