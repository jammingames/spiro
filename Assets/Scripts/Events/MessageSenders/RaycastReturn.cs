using UnityEngine;

public class RaycastReturn : MonoBehaviour
{
		public LayerMask mask;
		Camera cam;

		void Start ()
		{
				cam = ORef.instance.secondCam;
		}

		void Update ()
		{
				if (Input.GetMouseButtonDown (0)) {
				
						var ray = cam.ScreenPointToRay (Input.mousePosition);
						var point = cam.ScreenToWorldPoint (Input.mousePosition);
						RaycastHit hit;
						
						
						if (Physics.Raycast (ray, out hit, 1000, mask)) {
								
								hit.collider.BroadcastMessage ("OnClicked", Vector3.one, SendMessageOptions.DontRequireReceiver);
//								print ("sent on click broadcast to" + hit.collider);
						}
						if (!Physics.Raycast (ray, out hit, 1000, mask) && DrawLines.showGUI == true) {
								
								G.TriggerGamePause (false);
						
						}
				
				}
				if (Input.GetMouseButtonUp (0) && !DrawLines.showGUI) {
						//	G.TriggerGamePause (true);
				}
		}
}
