﻿using UnityEngine;

public class DraggableNew : MonoBehaviour
{
		public bool fixX;
		public bool fixY;
		Vector3 dragPos;
		float lastPos = 0;
		public Camera cam;
		bool dragging;

		void Update ()
		{
				if (Input.GetMouseButton (0)) {
						
						var ray = cam.ScreenPointToRay (Input.mousePosition);
						RaycastHit hit;
						if (GetComponent<Collider>().Raycast (ray, out hit, 1000)) {
								
								dragging = true;
							
								
								var point = cam.ScreenToWorldPoint (Input.mousePosition);

				
								point = GetComponent<Collider>().ClosestPointOnBounds (point);
								dragPos = point;
								if (GetComponent<Renderer> ())
										SendMessage ("OnDrag", Vector3.one - (dragPos - GetComponent<Renderer>().bounds.min) / GetComponent<Renderer>().bounds.size.y);
						} else if (!GetComponent<Collider>().Raycast (ray, out hit, 1000))
								dragging = false;
				
				}
				if (Input.GetMouseButtonUp (0)) {
						if (dragging)
								transform.parent.BroadcastMessage ("SendColor", SendMessageOptions.DontRequireReceiver);
				
						dragging = false;
						//print ("in get mouse up digabble broadcasts on color change. I believe i misused this");
				}
				//if (dragging && Input.GetMouseButton (0)) {
				
					
				
				
				//}
						
		}

		void SetDragPoint (Vector3 point)
		{
				
		
				if (GetComponent<Renderer> ())
						point = (Vector3.one - point) * GetComponent<Renderer>().bounds.size.y + GetComponent<Renderer>().bounds.min;
			
				dragPos = point;

		}
}
