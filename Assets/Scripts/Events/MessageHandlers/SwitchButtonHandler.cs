﻿using UnityEngine;
using System.Collections;

public class SwitchButtonHandler : MonoBehaviour
{
		public SpriteRenderer spr;
		public Sprite sprite, sprite2;

		void OnButtonSwitch (bool isTrue)
		{
//				D.log ("received message");
				if (spr.sprite == sprite) {
				
						spr.sprite = sprite2;
					
				} else if (spr.sprite == sprite2) {
				
						spr.sprite = sprite;
					
				}
				//	D.log ("dragging");
		}
}
