﻿using UnityEngine;
using System.Collections;

public class OnSwapButton : MonoBehaviour
{
		bool firstSprite = true;
		bool inTransition = false;

		void Start ()
		{
				G.OnSwitchButton += OnSwitchButton;
		}

		void SwapSprite ()
		{
				if (!inTransition) {
						
				
						G.TriggerStartSwitchButton (firstSprite);
						inTransition = true;
						firstSprite = !firstSprite;
				}
						
				//G.TriggerSwitchButton (!firstSprite);
		}

		void OnSwitchButton (bool isBackground)
		{
				D.log ("is done transitioning");
				inTransition = false;
		}
}
