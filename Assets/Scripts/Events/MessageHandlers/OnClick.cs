﻿using UnityEngine;
using System.Collections;

public class OnClick : MonoBehaviour
{
		public string MessageToSend;

		void OnClicked ()
		{
				transform.BroadcastMessage (MessageToSend, SendMessageOptions.DontRequireReceiver);
				print ("was clicked, sent " + MessageToSend);
		}

		public void ChangeMessage (string message)
		{
				MessageToSend = message;
		}
}
