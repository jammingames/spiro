﻿using UnityEngine;
using System.Collections;

public class SwitchMessageOnEvent : MonoBehaviour
{
		public string messageAfterPause;
		public string messageOnResume;

		void Start ()
		{
				G.OnGamePause += OnPause;
		}

		void OnPause (bool isPaused)
		{
	
				if (isPaused)
						BroadcastMessage ("ChangeMessage", messageAfterPause, SendMessageOptions.DontRequireReceiver);
				else if (!isPaused)
						BroadcastMessage ("ChangeMessage", messageOnResume, SendMessageOptions.DontRequireReceiver);
		}
}
