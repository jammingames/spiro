﻿using UnityEngine;
using System.Collections;
using System;

public class MuthaFuckinRunOnPauseEvent : MonoBehaviour
{
		public bool relativeX, relativeY, relativeZ;
		public Vector3 hidingSpot;
		public float runFasterrr;
		public EaseType fancyFeet;
		private Vector3 originalPos;

		void Start ()
		{
				
				//	MethodInfo mi = this.GetType ().GetMethod (eventName);
				G.OnReposition += OnRepositionEvent;
				
				G.OnGamePause += OnPauseEvent;
				//if (this.GetComponent<Renderer> () != null)
				//	this.renderer.enabled = false;
				
				//irection = new Vector3 (direction.x < 0 ? Mathf.Clamp (direction.x, -maxForce, -maxForce / 2) : Mathf.Clamp (direction.x, maxForce / 2, maxForce), direction.y < 0 ? Mathf.Clamp (direction.y, -maxForce, -maxForce / 2) : Mathf.Clamp (direction.y, maxForce / 2, maxForce), direction.z);
				originalPos = transform.position;
				hidingSpot = new Vector3 (relativeX == true ? transform.position.x + hidingSpot.x : hidingSpot.x, relativeY == true ? transform.position.y + hidingSpot.y : hidingSpot.y, relativeZ == true ? transform.position.z + hidingSpot.z : hidingSpot.z);
				MoveAway ();
		}

		void OnRepositionEvent (Vector3 pos)
		{
				originalPos = pos;
		}

		void OnPauseEvent (bool isPaused)
		{
				if (isPaused)
						MoveBack ();
				else {
						MoveAway ();
				}
		}

		void MoveBack ()
		{
				
				//	if (this.GetComponent<Renderer> () != null)
				//		this.renderer.enabled = true;
				StartCoroutine (transform.MoveTo (originalPos, runFasterrr, fancyFeet, null));
		}

		void MoveAway ()
		{
				StartCoroutine (transform.MoveTo (hidingSpot, runFasterrr, fancyFeet, () => {
						DrawLines.showGUI = false;	
						//		if (this.GetComponent<Renderer> () != null)
						//			this.renderer.enabled = false;
				}));
		}

		void MoveAway (Vector3 pos)
		{
				StartCoroutine (transform.MoveTo (pos, runFasterrr, fancyFeet, () => {
						//		if (this.GetComponent<Renderer> () != null)
						//			this.renderer.enabled = false;
				}));
		}
}
