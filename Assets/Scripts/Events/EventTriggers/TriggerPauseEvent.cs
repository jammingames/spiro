﻿using UnityEngine;
using System.Collections;
using System;

public class TriggerPauseEvent : MonoBehaviour
{
	public bool triggerOnStart;

	void Start ()
	{
		//if (triggerOnStart)
		G.TriggerGamePause (false);
		print ("triggering");
	}

	public void TriggerPause ()
	{
		G.TriggerGamePause (!DrawLines.showGUI);
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Jump")) {
			G.TriggerGamePause (!DrawLines.showGUI);
		}
	}
}