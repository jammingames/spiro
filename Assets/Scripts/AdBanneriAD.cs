﻿using UnityEngine;
using System.Collections;

public class AdBanneriAD : MonoBehaviour
{

#if UNITY_IPHONE
		private ADBannerView banner = null;
		void Start ()
		{
				banner = new ADBannerView (ADBannerView.Type.Banner, ADBannerView.Layout.Top);
				ADBannerView.onBannerWasClicked += OnBannerClicked;
				ADBannerView.onBannerWasLoaded += OnBannerLoaded;
				
		}
		void OnBannerClicked ()
		{
				Debug.Log ("Clicked!\n");
		}
		void OnBannerLoaded ()
		{
				Debug.Log ("Loaded!\n");
				print (banner.size);
				//G.TriggerResize (banner.size.y);
				banner.visible = true;
		}
#endif
}
