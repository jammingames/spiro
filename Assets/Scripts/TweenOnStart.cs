﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class TweenOnStart : MonoBehaviour
{
		public EaseType easer;
		public Vector3 target;
		public float duration;
		public Vector3 thisRotation;

		void Awake ()
		{
				Quaternion newRot = Quaternion.Euler (thisRotation);
				//		StartCoroutine (transform.MoveTo (target, duration, easer, null));
				StartCoroutine (transform.RotateTo (newRot, duration, easer, null));
										
		}
}
