using UnityEngine;

public class ColorHuePicker : MonoBehaviour
{
		void Start ()
		{

				//OnDrag (new Vector3 (-200, 0, 0));
		}

		void SetColor (HSBColor color)
		{
				SendMessage ("SetDragPoint", new Vector3 (1 - color.h, 1, 1));
		}

		void OnDrag (Vector3 point)
		{
				
				transform.parent.BroadcastMessage ("SetHue", 1 - point.y);
				//	transform.parent.BroadcastMessage ("SetSaturationBrightness", new Vector2 (1, 1));
		}

		
}
