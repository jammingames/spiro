﻿using UnityEngine;
using System.Collections;

public class ChangeSprite : MonoBehaviour
{
		public SpriteRenderer spr;
		public Sprite sprite, sprite2;

		void Start ()
		{
				G.OnSwitchButton += OnSwapSprite;
				G.OnStartSwitchButton += FinishedSwap;
		}

		void OnSwapSprite (bool isBackground)
		{
//				print ("received message");
				if (isBackground) {
				
						spr.sprite = sprite;
					
				} else if (!isBackground) {
				
						spr.sprite = sprite2;
					
				}
				
				G.OnSwitchButton -= OnSwapSprite;
				//	print ("dragging");
		}

		void FinishedSwap (bool isBackground)
		{
				G.OnSwitchButton += OnSwapSprite;
		}
}
