﻿using UnityEngine;
using System.Collections;

public class GradientColor : MonoBehaviour
{
		public Color col1, col2;
		Mesh mesh;
		Vector2[] uv;
		Color[] colors;
		public float offset;

		void Start ()
		{
	
				mesh = GetComponent<MeshFilter> ().mesh;
				uv = mesh.uv;
				colors = new Color[uv.Length];
 
				// Instead if vertex.y we use uv.x
				//classify = (input > 0) ? "positive" : "negative";
				
				for (var i = 0; i < uv.Length; i++) {
						
						//print (uv.Length + " " + k);
						//		uv [i].y / offset
						colors [i] = Color.Lerp (col1, col2, uv [i / 2].y); 
				}
 
				mesh.colors = colors;
		}

		void Update ()
		{
				ChangeColours ();
				
				
		}

		void ChangeColours ()
		{
				mesh = GetComponent<MeshFilter> ().mesh;
				uv = mesh.uv;
				colors = new Color[uv.Length];
				for (var i = 0; i < uv.Length; i++) {
					
						colors [i] = Color.Lerp (col1, col2, uv [Mathf.Clamp (i * 2, 0, uv.Length - 1)].y); 
						//colors [i] = Color.Lerp (col1, col2, uv [i].y); 
				}
						
				mesh.colors = colors;
		}
}
