using UnityEngine;

public class ColorSaturationBrightnessPicker : MonoBehaviour
{
		/*
If Click down on hue collider, grab current 
*/
		public Material backgroundMaterial;

		void Start ()
		{
				HSBColor newCol = new HSBColor ((ORef.instance.dlines.colorStart));
				SetHue (newCol.h);
		}

		void SetColor (HSBColor color)
		{
				backgroundMaterial.SetColor ("_Color", new HSBColor (1 - color.h, 1, 1).ToColor ());
				SendMessage ("SetDragPoint", new Vector3 (1 - color.s, 1 - color.b, 0));
		}

		void OnDrag (Vector3 point)
		{
				//	transform.parent.BroadcastMessage ("SetHue", 1 - point.x);
				transform.parent.BroadcastMessage ("SetSaturationBrightness", new Vector2 (1 - point.x, 1 - point.y));
		}

		void SetHue (float hue)
		{

				backgroundMaterial.SetColor ("_Color", new HSBColor (hue, 1, 1).ToColor ());
				
		}
}
