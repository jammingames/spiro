using UnityEngine;

public class ColorIndicator : MonoBehaviour
{
		static HSBColor hsbcolor;
		public Material mat1, mat2;
		bool currentColor = true;

		void Start ()
		{
		
				//G.OnSwitchColor += OnSwapColor;
				hsbcolor = HSBColor.FromColor (ORef.instance.dlines.colorStart);
				//hsbEndColor = HSBColor.FromColor (ORef.instance.dlines.colorEnd);
				//hsbcolor = HSBColor.FromColor (renderer.sharedMaterial.GetColor ("_Color"));

				//transform.parent.BroadcastMessage ("SetColor", hsbcolor);
				GetComponent<Renderer>().sharedMaterial.SetColor ("_Color", hsbcolor.ToColor ());
		}

		void ApplyColor ()
		{
				GetComponent<Renderer>().sharedMaterial.SetColor ("_Color", hsbcolor.ToColor ());
				
				//transform.parent.BroadcastMessage ("OnColorChange", hsbcolor, SendMessageOptions.DontRequireReceiver);
				
				
		}

		void SendColor ()
		{
			
				G.TriggerColorChange (hsbcolor.ToColor (), hsbcolor.ToColor ());
		}

		void OnSwapColor (bool firstColor)
		{
				//	if (firstColor)
				GetComponent<Renderer>().sharedMaterial = mat1;
				//	else
				//			renderer.sharedMaterial = mat2;
						
				//	currentColor = !firstColor;
		}

		void SetHue (float hue)
		{
				
				hsbcolor.h = hue;
				ApplyColor ();
		}

		void SetSaturationBrightness (Vector2 sb)
		{		
				
				hsbcolor.s = sb.x;
				hsbcolor.b = sb.y;
				hsbcolor.a = 1;
				ApplyColor ();
		}
}
