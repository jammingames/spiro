﻿using UnityEngine;
using System.Collections;

public class StateTest : MonoBehaviour
{
		FSM states = new FSM ();

		void Start ()
		{
				//		states.PushState (FirstState);
		}

		void Update ()
		{
				//		states.DoState ();
		}

		public void FirstState ()
		{
				print ("first state");
				states.PopState ();
				states.PushState (SecondState);
		}

		public void SecondState ()
		{
				print ("second state");
				states.PopState ();
				states.PushState (FirstState);
		}
}
/*    public var position   :Vector3D;
    public var velocity   :Vector3D;
    public var brain      :FSM;
 
    public function Ant(posX :Number, posY :Number) {
        position    = new Vector3D(posX, posY);
        velocity    = new Vector3D( -1, -1);
        brain       = new FSM();
 
        // Tell the brain to start looking for the leaf.
        brain.setState(findLeaf);
    }
 
    /**
     * The "findLeaf" state.
     * It makes the ant move towards the leaf.
     */
/*
    public function findLeaf() :void {
    }
 
    
    public function goHome() :void {
    }
 
    
    public function runAway() :void {
    }
 
    public function update():void {
        // Update the FSM controlling the "brain". It will invoke the currently
        // active state function: findLeaf(), goHome() or runAway().
        brain.update();
 
        // Apply the velocity vector to the position, making the ant move.
        moveBasedOnVelocity();
    }*/