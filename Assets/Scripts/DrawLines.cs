using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DrawLines : MonoBehaviour
{
		public LayerMask mask;
		public Material material, color1, color2;
		public static bool showGUI = false;
		private float speed = 260f;
		private Vector3 curr;
		private Vector3 last = new Vector3 (0, 0, -100.0f);
		private Vector3 upRot, sideRot;
		public float rotationSpeed;
		public int canvasIndex = 0;
		private float lineSizeLarge;
		private float lineSizeSmall;
		public Color colorEnd, colorStart;
		public Camera cam, camTwo;
		public List<AwesomeLineRenderer> lines;
		public List<LineParams> savedLines = new List<LineParams> ();
		public List<Vector3> lineVertPositions;
		private Vector3 accel;
		private Vector3 offsetAccel;
		private float lineSize;
		//public List<Vector3> points;
		private bool shouldCreate = true;
		private bool canDraw = true;
		public AwesomeLineRenderer currentLineReference;



    Vector3 lastInputPos;
    Vector3 deltaInput;
    void Awake ()
		{
				
				camTwo = ORef.instance.secondCam;
				Application.targetFrameRate = 60;
				Input.multiTouchEnabled = true;
				lines = new List<AwesomeLineRenderer> (100);
		}

		void Start ()
		{
				
				G.OnColorChange += OnColorChangeEvent;
				G.OnGamePause += OnGamePause;
				G.OnSwitchButton += OnSwitchButton;
				G.OnClear += OnClear;
				G.OnSave += OnSave;
				G.OnReset += OnReset;
				G.OnCalibrate += OnCalibrate;
				
				colorStart = new Color (0.8f, 0.8f, 0.8f, 0.04f);
				colorEnd = new Color (1f, 1f, 1f, 1f);
				color1.color = colorEnd;
				lineSize = lineSizeLarge = lineSizeSmall = 0.015f;
		}


		void SaveScores ()
		{
				foreach (AwesomeLineRenderer line in lines) {
						savedLines.Add (line.SaveLine ());
				}

				//Get a binary formatter
				var b = new BinaryFormatter ();
				//Create an in memory stream
				var m = new MemoryStream ();
				var f = File.Create (Application.persistentDataPath + "/savedLineData.dat");
				//Save the scores
				b.Serialize (f, savedLines);
				f.Close ();
		}

		void LoadScore ()
		{
				if (File.Exists (Application.persistentDataPath + "/savedLineData.dat")) {
						//Binary formatter for loading back
						var b = new BinaryFormatter ();
						//Get the file
						var f = File.Open (Application.persistentDataPath + "/savedLineData.dat", FileMode.Open);
						//Load back the scores
						savedLines = (List<LineParams>)b.Deserialize (f);
						f.Close ();
				}
		}

		void OnApplicationPause (bool pauseStatus)
		{
			
				upRot = Vector3.zero;
				
				sideRot = Vector3.zero;
				
				
		}

		void Update ()
		{
				if (Input.GetAxis ("Vertical") != 0)
						sideRot = -cam.transform.right * -Input.GetAxis ("Vertical") * rotationSpeed * 16 * Time.deltaTime;
				if (Input.GetAxis ("Horizontal") != 0)
						upRot = -cam.transform.up * Input.GetAxis ("Horizontal") * rotationSpeed * 8 * Time.deltaTime;

				if (Input.touchCount >= 2) {
						canDraw = false;
						// Get movement of the finger since last frame
						Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;
						print (touchDeltaPosition.magnitude);
						if (touchDeltaPosition.magnitude < 1.5f)
								touchDeltaPosition = Vector3.ClampMagnitude (touchDeltaPosition, touchDeltaPosition.magnitude / 4);
						upRot = -cam.transform.up * touchDeltaPosition.x * rotationSpeed * 8 * Time.deltaTime;
						sideRot = -cam.transform.right * -touchDeltaPosition.y * rotationSpeed * 16 * Time.deltaTime;
						
						if (Input.GetTouch (0).phase == TouchPhase.Ended || Input.GetTouch (1).phase == TouchPhase.Ended || Input.GetTouch (1).phase == TouchPhase.Canceled || Input.GetTouch (0).phase == TouchPhase.Canceled) {

								canDraw = true;
								if (Input.GetTouch (0).deltaPosition.magnitude < 2f || Input.GetTouch (1).deltaPosition.magnitude < 2f) {
										upRot = Vector3.zero;
										sideRot = Vector3.zero;
								}
						}
						
				}		
				
				//	if (upRot.sqrMagnitude <= 0.16f)
				//			upRot = Vector3.zero;
				//	if (sideRot.sqrMagnitude <= 0.16f)
				//			sideRot = Vector3.zero;
				transform.Rotate (sideRot + upRot, Space.World);


					
				if (canDraw) {
								
						var ray = camTwo.ScreenPointToRay (Input.mousePosition);
						RaycastHit hit;
						
						
						if (!Physics.Raycast (ray, out hit, 1000, mask)) {
								
								if (Input.GetMouseButton (0) && !showGUI && Input.touchCount < 2) {
										GetClickPoint (Input.mousePosition);
								} else
										last.z = -100.0f;
				
								if (Input.GetMouseButtonUp (0) && !showGUI && Input.touchCount < 2)
										shouldCreate = true;
								if (Input.GetMouseButtonDown (0) && !showGUI && Input.touchCount < 2 && shouldCreate) {
										shouldCreate = false;
										last = curr;
										MakeLine ();
								}
						}
				}
		}

	#region drawing

		void MakeLine ()
		{
				var newLine = createLine (curr, curr, lineSizeLarge);
				lines.Add (newLine);
				currentLineReference = newLine;

		}

		private void GetClickPoint (Vector3 pos)
		{
				curr = Camera.main.ScreenToWorldPoint (new Vector3 (pos.x, pos.y, Camera.main.transform.position.z * -1.0f));
				curr = transform.InverseTransformPoint (curr);
			
				if (last.z != -100.0f) {
						if (!shouldCreate)
								currentLineReference.AddToLine (curr);
				}
				last = curr;
		}

		private AwesomeLineRenderer createLine (Vector3 start, Vector3 end, float lineSize)
		{	
				GameObject canvas = new GameObject ("line" + lines.Count); 
				canvas.transform.parent = transform;
				canvas.transform.rotation = transform.rotation;	
				var newLine = (LineRenderer)canvas.AddComponent<LineRenderer> ();
				AwesomeLineRenderer lr = (AwesomeLineRenderer)canvas.AddComponent<AwesomeLineRenderer> ();
				lr.CreateNew (material, colorStart, colorEnd, lineSize, start, end);
				return lr;
		}
		

	#endregion

	#region Event methods
	

		void OnSwitchButton (bool isBackground)
		{
				if (isBackground) {
						D.log ("drawlines color removed ");		
						G.OnColorChange -= OnColorChangeEvent;
				} else {
						D.log ("drawlines color added ");
						G.OnColorChange += OnColorChangeEvent;
				}
		}

		void OnColorChangeEvent (Color col, Color col2)
		{
				colorEnd = col;
				colorStart = new Color (col.r - 0.05f, col.g - 0.05f, col.b - 0.05f, 0.02f);
				
				
		}

		void OnGamePause (bool isPaused)
		{
				if (isPaused)
						showGUI = true;
				else
						showGUI = false;
				shouldCreate = true;
			
		}

		void OnClear ()
		{
				if (lines.Count > 0) {
						
						foreach (AwesomeLineRenderer line in lines)
								Destroy (line.gameObject);
						canvasIndex = 0;
						lines.Clear ();
				}
				currentLineReference = null;
				shouldCreate = true;
		}

		void OnSave ()
		{
				//ScreenshotScript.Capture ();
		}

		void OnReset ()
		{
				transform.localRotation = Quaternion.identity;
		}

		void OnCalibrate ()
		{
				offsetAccel = Input.acceleration;
		}
	#endregion


}



