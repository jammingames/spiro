﻿using UnityEngine;
using System.Collections;

public class RotateScript : MonoBehaviour
{
	public LineRenderer mat;
	public float rotateAmmount;
	public float duration;
	public float delay = 1.0f;
	public AnimationCurve curve;

	IEnumerator Start ()
	{
		yield return new WaitForSeconds (delay);
		print ("done delay");

		yield return StartCoroutine (mat.FadeAlpha (0, duration, curve, null));
		print ("done fade");
//		Destroy (this.gameObject);
//		Fader.instance.FadeAlphaOut (mat, duration, () => {
//			Destroy (this.gameObject);
//			Fader.instance.FadeAlphaIn (mat, 0.1f, null);
//		});
	}

	void Update ()
	{
		transform.RotateAround (transform.up, rotateAmmount * Time.deltaTime);
	}
}
