﻿using UnityEngine;
using System.Collections;
using System.Security.Policy;

public class TriggerMoveEvent : MonoBehaviour
{
	bool startState = true;

	void Awake ()
	{
		//G.OnGamePause += TriggerMoveEvent;
	}

	void Update ()
	{	
						
		if (Input.touchCount > 0) {
			if (Input.GetTouch (0).phase == TouchPhase.Moved) {
				float touchDeltaPosition = Input.GetTouch (0).deltaPosition.y;
				if (Input.GetTouch (0).phase == TouchPhase.Ended) {
					if (touchDeltaPosition < 0) {
						G.TriggerGamePause (false);
					} else
						G.TriggerGamePause (true);
				}
			
			}
		}
		if (Input.GetKeyDown (KeyCode.T)) {
			G.TriggerGamePause (!startState);
			startState = !startState;
		}
				
	}
	/*
		void TriggerMoveEvent()
		{
				
		}
		*/
}
